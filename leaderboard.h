#ifndef LEADEBOARD_H
#define LEADEBOARD_H
#include "consts.h"
#include <SDL/SDL.h>

class Leaderboard {
private:
  char nickname[LEADERBOARDSIZE][11];
  unsigned points[LEADERBOARDSIZE];
  void loadLeaderboard();
  void saveLeaderboard();

public:
  Leaderboard();
  bool comparePoints(unsigned points);
  void putIntoLeaderboard(char nickname[10], unsigned points);
  void printLeaderboard(SDL_Surface *screen, SDL_Surface *charset);};

#endif // LEADEBOARD_H
