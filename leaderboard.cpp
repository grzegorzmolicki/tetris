#include "leaderboard.h"
#include "template.h"
#include <string.h>
#include <fstream>
#include <iostream>

void Leaderboard::loadLeaderboard() {
  std::ifstream inputFile(LEADERBOARD);
  char nickname[11];
  unsigned points;
  for(int i = 0; i < LEADERBOARDSIZE && !inputFile.eof(); i++) {
      for(int k = 0; k < 10; k++) {
          inputFile >> nickname[k];
        }
      inputFile >> points;
      nickname[10] = '\0';
      strcpy(this->nickname[i], nickname);
      this->points[i] = points;
    }
  inputFile.close();
}

void Leaderboard::saveLeaderboard() {
  std::ofstream outputFile(LEADERBOARD);
  for(int i = 0; i < LEADERBOARDSIZE; i++) {
      outputFile << this->nickname[i] << ' ';
      outputFile << this->points[i] << '\n';
    }
  outputFile.close();
}


Leaderboard::Leaderboard() {
  char name[] = {"----------"};
  for(int i = 0; i < LEADERBOARDSIZE; i++) {
      strcpy(this->nickname[i], name);
      this->points[i] = 0;
    }
  loadLeaderboard();
}

bool Leaderboard::comparePoints(unsigned points) {
  for(int i = 0; i < LEADERBOARDSIZE; i++)
      if(this->points[i] < points) return true;

  return false;
}

void Leaderboard::putIntoLeaderboard(char nickname[11], unsigned points) {
  int pos;
  for(int i = 0; i < LEADERBOARDSIZE; i++) {
      if(this->points[i] < points) {
          pos = i;
          break;
        }
    }
  for(int i = LEADERBOARDSIZE-1; i > pos; i--) {
      for(int k = 0; k < 11; k++) this->nickname[i][k] = this->nickname[i-1][k];
      this->points[i] = this->points[i-1];
    }

  strcpy(this->nickname[pos], nickname);
  this->points[pos] = points;
  this->saveLeaderboard();
}

void Leaderboard::printLeaderboard(SDL_Surface *screen, SDL_Surface *charset) {
  for(int i = 0; i < LEADERBOARDSIZE; i++) {
      char text[32];
      sprintf(text, "%s %d", this->nickname[i], this->points[i]);
      DrawString(screen, 112, 16*i+((12*BLOCKWIDTH-128)/2), text, charset);
    }
}
