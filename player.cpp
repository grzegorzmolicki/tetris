#include "player.h"
#include "consts.h"
#include "template.h"

Player::Player() {
  this->lastLines = 0;
  this->linesCompleted = 0;
  this->level = 0;
  this->points = 0;
  this->levelTimeCounter = SDL_GetTicks();
  this->timesSaved = 0;
}

void Player::addPoints(int lines) {
  if(this->lastLines == 4) this->points += 1200*(this->level+1);
  if(lines > 0) {
      this->points += 100*pow(2, lines-1)*(this->level+1);
      this->lastLines = lines;
      this->linesCompleted += lines;
    }
}

void Player::printStats(SDL_Surface *screen, SDL_Surface *charset) {
  char text[128];
  sprintf(text, "Points: %d", this->points);
  DrawString(screen, 12*BLOCKWIDTH+20, 100, text, charset);

  sprintf(text, "Lines: %d", this->linesCompleted);
  DrawString(screen, 12*BLOCKWIDTH+20, 116, text, charset);

  sprintf(text, "Level: %d", this->level);
  DrawString(screen, 12*BLOCKWIDTH+20, 132, text, charset);
}

void Player::nextLevel() {
  this->level++;
  this->levelTimeCounter = SDL_GetTicks();
  if(this->level > 10) this->level = 10;
}

void Player::checkLevel() {
  if(SDL_GetTicks()-this->levelTimeCounter > DELTAT*1000) {
      this->level++;
      this->levelTimeCounter = SDL_GetTicks();
      this->nextLevel();
    }
}

void Player::nextSave() {
  this->timesSaved++;
}

unsigned Player::getLevel() {
  return this->level;
}

unsigned Player::getTimesSaved() {
  return this->timesSaved;
}

void Player::reset() {
  this->lastLines = 0;
  this->linesCompleted = 0;
  this->level = 0;
  this->points = 0;
  this->levelTimeCounter = SDL_GetTicks();
  this->timesSaved = 0;
}

void Player::loadGameStats(unsigned stats[6]) {
  /* well, the trick to avoid much unnecessary work is to write everything in an array and load as shown:
   * 0: lastLines
   * 1: linesCompleted
   * 2: level
   * 3: points
   * 4: levelTimeCounter
   * 5: timesSaved
   * */
  this->lastLines = stats[0];
  this->linesCompleted = stats[1];
  this->level = stats[2];
  this->points = stats[3];
  this->levelTimeCounter = stats[4];
  this->timesSaved = stats[5];
}

unsigned Player::getLastLines() {
  return this->lastLines;
}

unsigned Player::getLinesCompleted() {
  return this->linesCompleted;
}

unsigned Player::getPoints() {
  return this->points;
}

unsigned Player::getLevelTimeCounter() {
  return this->levelTimeCounter;
}


