#include <SDL2/SDL_main.h>
#include <SDL2/SDL.h>
#include <fstream>
#include <iostream>
#include "consts.h"
#include "currentblock.h"
#include "player.h"
#include "template.h"
#include "ai.h"
#include "leaderboard.h"

void drawGrid(SDL_Surface *screen, int grid[ROWS][COLS]) {
  unsigned colors[] = {
    SDL_MapRGB(screen->format, 0xFF, 0x00, 0x00), // RED
    SDL_MapRGB(screen->format, 0xD2, 0x69, 0x1E), // CHOCOLATE, choco choco choco choco chocolaaaateeee
    SDL_MapRGB(screen->format, 0x00, 0x00, 0xFF), // BLUE
    SDL_MapRGB(screen->format, 0xFF, 0xFF, 0x00), // YELLOW
    SDL_MapRGB(screen->format, 0x00, 0x80, 0x00), // GREEN
    SDL_MapRGB(screen->format, 0xFF, 0x45, 0x00), // ORANGE RED
    SDL_MapRGB(screen->format, 0xFF, 0xFF, 0xFF), // WHITE
    SDL_MapRGB(screen->format, 0xD3, 0xD3, 0xD3), // GRAY
    SDL_MapRGB(screen->format, 0x00, 0x00, 0x00)}; // BLEK

  for(int i = 0; i < ROWS; i++)
    for(int j = 0; j < COLS; j++) {
        if(grid[i][j] == 0) DrawRectangle(screen, j*BLOCKWIDTH+BLOCKWIDTH, i*BLOCKHEIGHT+BLOCKHEIGHT, BLOCKWIDTH, BLOCKHEIGHT, colors[8], colors[8]);
        else DrawRectangle(screen, j*BLOCKWIDTH+BLOCKWIDTH, i*BLOCKHEIGHT+BLOCKHEIGHT, BLOCKWIDTH, BLOCKHEIGHT, colors[8], colors[grid[i][j]-1]);
      }
}

void reset(int grid[ROWS][COLS], CurrentBlock *block, Player *player) {
  for(int i = 0; i < ROWS; i++) {
      for(int j = 0; j < COLS; j++)
        grid[i][j] = 0;
    }
  block->reset();
  player->reset();
}

bool didLost(int grid[ROWS][COLS]) {
  for(int j = 0; j < COLS; j++)
    if(grid[0][j] != 0) return true;

  return false;
}

// Draws playground border
void drawBorder(SDL_Surface *screen) {
  int gray = SDL_MapRGB(screen->format, 0xD3, 0xD3, 0xD3); // GRAY

  for(int i = 0; i < COLS+2; i++)
    for(int j = 0; j < ROWS+2; j++)
      if(i == 0 || j == 0 || i == COLS+1 || j == ROWS+1) DrawRectangle(screen, i*BLOCKHEIGHT, j*BLOCKWIDTH, BLOCKHEIGHT, BLOCKWIDTH, gray, gray);
}

void saveGame(int grid[ROWS][COLS], Player *player, CurrentBlock *block) {
  if(player->getTimesSaved() < 2) {
      std::fstream out(SAVEFILE);
      out << block->getPosition().x << ' ' << block->getPosition().y << ' ' << block->getType() << ' ' << block->getRotation() << '\n';
      out << player->getLevel() << ' ' << player->getPoints() << ' ' << player->getLastLines() << ' ' << player->getLevelTimeCounter() << ' ';
      out << player->getLinesCompleted() << ' ' << player->getTimesSaved() << '\n';
      player->nextSave();

      for(int i = 0; i < ROWS; i++) {
          for(int j = 0; j < COLS; j++) {
              out << grid[i][j] << ' ';
            }
          out << '\n';
        }
      out.close();
    }
}

void loadGame(int grid[ROWS][COLS], Player *player, CurrentBlock *block) {
  std::fstream in(SAVEFILE);
  Point blockPosition;
  unsigned blockStats[2];
  in >> blockPosition.x >> blockPosition.y;
  in >> blockStats[0] >> blockStats[1];
  block->loadGame(blockPosition, blockStats);

  unsigned playerStats[6];
  for(int i = 0; i < 6; i++)
    in >> playerStats[i];

  player->loadGameStats(playerStats);

  for(int i = 0; i < ROWS; i++)
    for(int j = 0; j < COLS; j++)
      in >> grid[i][j];
}

int main() {
  bool aiOn = false, saved = false;
  bool pause = false;
  int grid[ROWS+1][COLS];
  for(int i = 0; i < ROWS+1; i++)
    for(int j = 0; j < COLS; j++)
      grid[i][j] = 0;
  for(int i = 0; i < COLS; i++)
    grid[ROWS][i] = 1;

  int quit, rc;
  SDL_Event event;
  SDL_Surface *screen, *charset;
  SDL_Texture *scrtex;
  SDL_Window *window;
  SDL_Renderer *renderer;

  if(SDL_Init(SDL_INIT_EVERYTHING) != 0) {
      printf("SDL_Init error: %s\n", SDL_GetError());
      return 1;
    }

  rc = SDL_CreateWindowAndRenderer(0, 0, SDL_WINDOW_FULLSCREEN_DESKTOP, &window, &renderer);
  if(rc != 0) {
      SDL_Quit();
      printf("SDL_CreateWindowAndRenderer error: %s\n", SDL_GetError());
      return 1;
    };

  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");
  SDL_RenderSetLogicalSize(renderer, SCREEN_WIDTH, SCREEN_HEIGHT);
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

  SDL_SetWindowTitle(window, "Tetris!");
  screen = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
  scrtex = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, SCREEN_WIDTH, SCREEN_HEIGHT);

  // blue color for background
  unsigned niebieski = SDL_MapRGB(screen->format, 0x00, 0x00, 0xFF);

  CurrentBlock *block = new CurrentBlock(screen, 1);
  Player *player = new Player();
  SDL_ShowCursor(SDL_DISABLE);

  // wczytanie obrazka cs8x8.bmp
  charset = SDL_LoadBMP("/home/greg/tetrisG/cs8x8.bmp");
  if(charset == NULL) {
      printf("SDL_LoadBMP(cs8x8.bmp) error: %s\n", SDL_GetError());
      delete player;
      delete block;
      SDL_FreeSurface(screen);
      SDL_DestroyTexture(scrtex);
      SDL_DestroyWindow(window);
      SDL_DestroyRenderer(renderer);
      SDL_Quit();
      return 1;
    };


  SDL_SetColorKey(charset, true, 0x000000);
  quit = 0;

  while(!quit) {
      if(!didLost(grid)) {
          if(!pause) {
              player->checkLevel();
              int lines = checkLine(grid);
              block->lowerBlock(grid, player, false);
              SDL_FillRect(screen, NULL, niebieski);
              drawGrid(screen, grid);
              block->drawBlock(screen);
              drawBorder(screen);

              player->addPoints(lines);
              player->printStats(screen, charset);

              SDL_UpdateTexture(scrtex, NULL, screen->pixels, screen->pitch);
              SDL_RenderCopy(renderer, scrtex, NULL, NULL);
              SDL_RenderPresent(renderer);
              saved = false;

              if(block->hasActionToMake()) {
                  block->makeAction(grid, player);
                } else {
                  while(SDL_PollEvent(&event)) {
                      switch(event.type) {
                        case SDL_KEYDOWN:
                          if(event.key.keysym.sym == SDLK_ESCAPE) quit = 1;
                          else if(event.key.keysym.sym == SDLK_DOWN) block->addActionToMake(LOWERACTION);
                          else if(event.key.keysym.sym == SDLK_LEFT) block->addActionToMake(MOVELEFTACTION);
                          else if(event.key.keysym.sym == SDLK_RIGHT) block->addActionToMake(MOVERIGHTACTION);
                          else if(event.key.keysym.sym == SDLK_SPACE) block->rotate(grid);
                          else if(event.key.keysym.sym == SDLK_p) pause = true;
                          else if(event.key.keysym.sym == SDLK_s) player->nextLevel();
                          else if(event.key.keysym.sym == SDLK_n) reset(grid, block, player);
                          else if(event.key.keysym.sym == SDLK_i) aiOn = !aiOn;
                          else if(event.key.keysym.sym == SDLK_z) saveGame(grid, player, block);
                          else if(event.key.keysym.sym == SDLK_l) loadGame(grid, player, block);
                          break;
                        case SDL_QUIT:
                          quit = 1;
                          break;
                        }
                    }
                  if(aiOn) {
                      makeAIMove(grid, block, screen, player);
                    }
                }
            } else {
              while(SDL_PollEvent(&event)) {
                  switch(event.type) {
                    case SDL_KEYDOWN:
                      if(event.key.keysym.sym == SDLK_ESCAPE) quit = 1;
                      else if(event.key.keysym.sym == SDLK_p) pause = false;
                      else if(event.key.keysym.sym == SDLK_n) block->reset();
                      else if(event.key.keysym.sym == SDLK_i) aiOn = ~aiOn;
                      break;
                    case SDL_QUIT:
                      quit = 1;
                      break;
                    }
                }
            }
        } else { // LOST GAME
          aiOn = false;
          char message[] = "Game over! [n] - One more time [Esc] - Quit";
          Leaderboard *leaderboard = new Leaderboard();
          char nickname[] = "----------";
          int cursor = 0;
          if(leaderboard->comparePoints(player->getPoints()) && !saved) { // Player qualifies to leaderboard. Get his nickname
              while(!saved && cursor < 10) {
                  while(SDL_PollEvent(&event)) {
                      switch(event.type) {
                        case SDL_KEYDOWN:
                          if(event.key.keysym.sym == SDLK_SPACE) saved = true;
                          else if(event.key.keysym.sym == SDLK_BACKSPACE) {
                              cursor--;
                              if(cursor < 0) cursor = 0;
                              nickname[cursor] = '-';
                            }
                          else if(event.key.keysym.sym > 96 && event.key.keysym.sym < 123) {
                              nickname[cursor] = (char)event.key.keysym.sym;
                              cursor++;
                            }
                          break;
                        }
                    }
                  char text[11];
                  text[10] = '\0';
                  strcpy(text, nickname);
                  DrawRectangle(screen, 0, 0, 12*BLOCKWIDTH, 22*BLOCKHEIGHT, 0, 0);
                  leaderboard->printLeaderboard(screen, charset);
                  DrawString(screen, (12*BLOCKWIDTH-128)/2, 400, text, charset);
                  SDL_UpdateTexture(scrtex, NULL, screen->pixels, screen->pitch);
                  SDL_RenderCopy(renderer, scrtex, NULL, NULL);
                  SDL_RenderPresent(renderer);
                }
              saved = true;
              leaderboard->putIntoLeaderboard(nickname, player->getPoints());
            }
          DrawRectangle(screen, 0, 0, 12*BLOCKWIDTH, 22*BLOCKHEIGHT, 0, 0);
          DrawString(screen, 0, 60, message, charset);
          leaderboard->printLeaderboard(screen, charset);
          SDL_UpdateTexture(scrtex, NULL, screen->pixels, screen->pitch);
          SDL_RenderCopy(renderer, scrtex, NULL, NULL);
          SDL_RenderPresent(renderer);
          while(SDL_PollEvent(&event)) {
              switch(event.type) {
                case SDL_KEYDOWN:
                  if(event.key.keysym.sym == SDLK_n) reset(grid, block, player);
                  else if(event.key.keysym.sym == SDLK_ESCAPE) quit = 1;
                  break;
                case SDL_QUIT:
                  quit = 1;
                  break;
                }
            }
          delete leaderboard;
        }
      SDL_Delay(FRAMEDELAY);
    }

  // zwolnienie powierzchni
  delete player;
  delete block;
  SDL_FreeSurface(charset);
  SDL_FreeSurface(screen);
  SDL_DestroyTexture(scrtex);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}
