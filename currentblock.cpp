#include "currentblock.h"
#include "player.h"
#include "consts.h"
#include <time.h>
#include "template.h"

CurrentBlock::CurrentBlock(SDL_Surface *screen, int type) {
  this->position.y = 0;
  this->position.x = 4*BLOCKWIDTH;
  generateBlock(type);
  this->type = type;
  this->t0 = SDL_GetTicks(); // Couldn't imagine better name for start time or time of last lower of block. Feel free to change this crap. Thanks from the mountain.
  this->actionToMake = 0;
  this->rotations = 0;
  this->colors[0] = SDL_MapRGB(screen->format, 0xFF, 0x00, 0x00); // RED
  this->colors[1] = SDL_MapRGB(screen->format, 0xD2, 0x69, 0x1E); // CHOCOLATE, choco choco choco choco chocolaaaateeee
  this->colors[2] = SDL_MapRGB(screen->format, 0x00, 0x00, 0xFF); // BLUE
  this->colors[3] = SDL_MapRGB(screen->format, 0xFF, 0xFF, 0x00); // YELLOW
  this->colors[4] = SDL_MapRGB(screen->format, 0x00, 0x80, 0x00); // GREEN
  this->colors[5] = SDL_MapRGB(screen->format, 0xFF, 0x45, 0x00); // ORANGE RED
  this->colors[6] = SDL_MapRGB(screen->format, 0xFF, 0xFF, 0xFF); // WHITE
  this->colors[7] = SDL_MapRGB(screen->format, 0xD3, 0xD3, 0xD3); // GRAY
  this->colors[8] = SDL_MapRGB(screen->format, 0x00, 0x00, 0x00); // BLACK
}

// Generates bricks based on inputBlockCode
void CurrentBlock::generateBlock(int inputBlockCode) {
  for(int i = 0; i < 4; i++)
    for(int j = 0; j < 4; j++)
      this->brick[i][j] = 0;

  switch(inputBlockCode) {
    case 1:
      this->brick[0][0] = 1;
      this->brick[1][0] = 1;
      this->brick[2][0] = 1;
      this->brick[3][0] = 1;
      break;
    case 2:
      this->brick[0][0] = 2;
      this->brick[0][1] = 2;
      this->brick[0][2] = 2;
      this->brick[1][1] = 2;
      break;
    case 3:
      this->brick[0][0] = 3;
      this->brick[1][0] = 3;
      this->brick[0][1] = 3;
      this->brick[1][1] = 3;
      break;
    case 4:
      this->brick[0][0] = 4;
      this->brick[1][0] = 4;
      this->brick[2][0] = 4;
      this->brick[2][1] = 4;
      break;
    case 5:
      this->brick[0][1] = 5;
      this->brick[1][1] = 5;
      this->brick[2][1] = 5;
      this->brick[2][0] = 5;
      break;
    case 6:
      this->brick[0][1] = 6;
      this->brick[0][2] = 6;
      this->brick[1][0] = 6;
      this->brick[1][1] = 6;
      break;
    case 7:
      this->brick[0][0] = 7;
      this->brick[0][1] = 7;
      this->brick[1][1] = 7;
      this->brick[1][2] = 7;
      break;
    }
}

int CurrentBlock::getType() {
  return this->type;
}

// Returns rightmost index of brick part
int CurrentBlock::getMaxRightIndex(int brick[4][4]) {
  int maxRight = 0;
  for(int i = 0; i < 4; i++)
    for(int j = 0; j < 4; j++)
      if((brick[i][j] != 0) && j > maxRight) maxRight = j;
  return maxRight;
}

void CurrentBlock::lowerBlock(int grid[ROWS+1][COLS], Player *player, bool instantMove) {
  if(((SDL_GetTicks()-this->t0) >= (BASEMOVELATENCY-(player->getLevel()*100))) || instantMove) {
      bool isLowerable = true;
      for(int i = 0; i < 4; i++)
        for(int j = 0; j < 4; j++)
          if(brick[i][j] != 0 && (grid[i+1+(this->position.y/BLOCKHEIGHT)][j+(this->position.x/BLOCKWIDTH)] != 0)) isLowerable = false;

      if(isLowerable) {
          this->t0 = SDL_GetTicks();
          this->position.y += 2+16*instantMove;
        } else {
          for(int i = 0; i < 4; i++)
            for(int j = 0; j < 4; j++)
              if(brick[i][j] != 0) grid[i+(this->position.y/BLOCKHEIGHT)][j+(this->position.x/BLOCKWIDTH)] = brick[i][j];
          chooseRandomBlock();
          this->actionToMake = NOACTIONS;
        }
    }
}

void CurrentBlock::moveDown(int grid[ROWS+1][COLS], Player *player) {
  bool isLowerable = true;
  while(isLowerable) {
      for(int i = 0; i < 4; i++)
        for(int j = 0; j < 4; j++)
          if(brick[i][j] != 0 && (grid[i+1+(this->position.y/BLOCKHEIGHT)][j+(this->position.x/BLOCKWIDTH)] != 0)) isLowerable = false;
      if(isLowerable) {
          this->lowerBlock(grid, player, true);
          this->position.y += BLOCKHEIGHT;
        }
    }
  this->t0 = 0;
  this->lowerBlock(grid, player, true);
}

// Move to the left or to the right
void CurrentBlock::move(int grid[ROWS+1][COLS], int dir) {
  bool isMovable = true;

  for(int i = 0; i < 4; i++)
    for(int j = 0; j < 4; j++)
      if(brick[i][j] != 0 && (grid[i+(int)ceil(((double)this->position.y/BLOCKHEIGHT))][j+(int)ceil(((double)this->position.x/BLOCKWIDTH)+dir)] != 0)) {
          isMovable = false;
          this->actionToMake = NOACTIONS;
        }

  if(isMovable) {
      this->position.x += dir*(BLOCKWIDTH/FRAMESPERMOVE);
      if(this->position.x < 0) this->position.x = 0;
      // If block doesn't fit inside playground, then it's pulled to the left.
      if(this->position.x > (COLS-getMaxRightIndex(this->brick)-1)*BLOCKWIDTH) this->position.x = (COLS-getMaxRightIndex(this->brick)-1)*BLOCKWIDTH;
      if(this->position.x % BLOCKWIDTH == 0) this->actionToMake = NOACTIONS;
    }
}

// Makes AI move. This simply speeds up moving block to the left or to the right, because animation is avoided.
void CurrentBlock::aiMove(int grid[ROWS+1][COLS], int dir) {
  bool isMovable = true;

  for(int i = 0; i < 4; i++)
    for(int j = 0; j < 4; j++)
      if(brick[i][j] != 0 && (grid[i+(int)ceil(((double)this->position.y/BLOCKHEIGHT))][j+(int)ceil(((double)this->position.x/BLOCKWIDTH)+dir)] != 0)) {
          isMovable = false;
          this->actionToMake = NOACTIONS;
        }

  if(isMovable) {
      this->position.x += dir*BLOCKWIDTH;
      if(this->position.x < 0) this->position.x = 0;
      if(this->position.x > (COLS-getMaxRightIndex(this->brick)-1)*BLOCKWIDTH) this->position.x = (COLS-getMaxRightIndex(this->brick)-1)*BLOCKWIDTH;
      if(this->position.x % BLOCKWIDTH == 0) this->actionToMake = NOACTIONS;
    }
}

void CurrentBlock::drawBlock(SDL_Surface *screen) {
  for(int i = 0; i < 4; i++)
    for(int j = 0; j < 4; j++)
      if(brick[i][j] != 0) DrawRectangle(screen, this->position.x+j*BLOCKWIDTH+BLOCKWIDTH, this->position.y+i*BLOCKHEIGHT+BLOCKHEIGHT, BLOCKWIDTH, BLOCKHEIGHT, this->colors[8], this->colors[this->type-1]);
}

void CurrentBlock::chooseRandomBlock() {
  srand(time(NULL));
  unsigned randomBlock = rand()%7+1;
  while(randomBlock == this->type) randomBlock = rand()%7+1;
  this->position.y = 0;
  this->position.x = 4*BLOCKWIDTH;
  this->type = randomBlock;
  this->rotations = 0;
  generateBlock(this->type);
}


// Rotation is generated by matrix transpose, then rows or cols (depends on which direction you want) are swaped(4 becomes 1,3 becomes 2, 2 becomes 3 and 1 becomes 4)
void CurrentBlock::rotate(int grid[ROWS+1][COLS]) {
  int tempBrick[4][4];
  int temponary[4][4];
  for(int i = 0; i < 4; i++)
    for(int j = 0; j < 4; j++)
      temponary[i][j] = brick[j][i];

  for(int i = 0; i < 4; i++)
    for(int j = 0; j < 4; j++)
      tempBrick[i][j] = temponary[3-i][j];

/* Both of these loops are trying to move block the closest left-top corner as possible
* Example:
* 0000
* 0000
* 0011
* 0011
* First loop will move block to the left side:
* 0000
* 0000
* 1100
* 1100
* Then up as far as possible:
* 1100
* 1100
* 0000
* 0000
*/

  while((tempBrick[0][0] == 0) && (tempBrick[1][0] == 0) && (tempBrick[2][0] == 0) && (tempBrick[3][0] == 0)) {
      for(int i = 0; i < 4; i++)
        for(int j = 1; j < 4; j++)
          tempBrick[i][j-1] = tempBrick[i][j];
      tempBrick[0][3] = tempBrick[1][3] = tempBrick[2][3] = tempBrick[3][3] = 0;
    }

  while((tempBrick[0][0] == 0) && (tempBrick[0][1] == 0) && (tempBrick[0][2] == 0) && (tempBrick[0][3] == 0)) {
      for(int i = 1; i < 4; i++)
        for(int j = 0; j < 4; j++)
          tempBrick[i-1][j] = tempBrick[i][j];
      tempBrick[3][0] = tempBrick[3][1] = tempBrick[3][2] = tempBrick[3][3] = 0;
    }
  // If block fits in playground, then rotation is possible (at this point!)
  bool isRotatable = true;
  if(this->position.x > (COLS-this->getMaxRightIndex(tempBrick)-1)*BLOCKWIDTH) isRotatable = false;

  // Is there any collision with block on playground? If so, then rotation is not possible
  for(int i = 0; i < 4; i++)
    for(int j = 0; j < 4; j++)
      if((tempBrick[i][j] != 0) && (grid[(this->position.y/BLOCKHEIGHT)+i][(this->position.x/BLOCKWIDTH)+j] != 0)) isRotatable = false;
  // Finally true, original, The Chosen One Block becomes rotated.
  if(isRotatable) {
      for(int i = 0; i < 4; i++)
        for(int j = 0; j < 4; j++)
          brick[i][j] = tempBrick[i][j];
    }
}

// Primitive buffer
void CurrentBlock::addActionToMake(int action) {
  this->actionToMake = action;
}

void CurrentBlock::makeAction(int grid[ROWS+1][COLS], Player *player) {
  if(this->actionToMake == LOWERACTION) this->lowerBlock(grid, player, true);
  else if(this->actionToMake == MOVELEFTACTION) this->move(grid, DIRLEFT);
  else if(this->actionToMake == MOVERIGHTACTION) this->move(grid, DIRRIGHT);
}

bool CurrentBlock::hasActionToMake() {
  return !(this->actionToMake == NOACTIONS);
}


void CurrentBlock::loadGame(Point position, unsigned stats[2]) {
  this->position = position;
  // posisition is obvious, but in stats:
  // 0 - type, 1 - t0, 2 - rotation;
  this->type = stats[0];
  this->generateBlock(this->type);
  for(unsigned r = 0; r < stats[1]; r++) {
      int temp[4][4];
      for(int i = 0; i < 4; i++)
        for(int j = 0; j < 4; j++)
          temp[i][j] = this->brick[j][i];

      for(int i = 0; i < 4; i++)
        for(int j = 0; j < 4; j++)
          brick[i][j] = temp[3-i][j];

      while((brick[0][0] == 0) && (brick[1][0] == 0) && (brick[2][0] == 0) && (brick[3][0] == 0)) {
          for(int i = 0; i < 4; i++)
            for(int j = 1; j < 4; j++)
              brick[i][j-1] = brick[i][j];
          brick[0][3] = brick[1][3] = brick[2][3] = brick[3][3] = 0;
        }

      while((brick[0][0] == 0) && (brick[0][1] == 0) && (brick[0][2] == 0) && (brick[0][3] == 0)) {
          for(int i = 1; i < 4; i++)
            for(int j = 0; j < 4; j++)
              brick[i-1][j] = brick[i][j];
          brick[3][0] = brick[3][1] = brick[3][2] = brick[3][3] = 0;
        }
    }

  this->t0 = SDL_GetTicks();
}

void CurrentBlock::reset() {
  this->t0 = SDL_GetTicks();
  this->position.x = 4*BLOCKWIDTH;
  this->position.y = 0;
  chooseRandomBlock();
}

unsigned CurrentBlock::getRotation() {
  return this->rotations%4;
}

Point CurrentBlock::getPosition() {
  return this->position;
}
