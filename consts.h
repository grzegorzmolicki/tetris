#ifndef CONSTS_H
#define CONSTS_H

#define COLS 10
#define ROWS 20
#define DELTAT 20
#define DIRRIGHT 1
#define DIRLEFT -1
#define NOACTIONS 0
#define FRAMEDELAY 6 // little delay for not killing cpu by many fps
#define LOWERACTION 1
#define BLOCKWIDTH 30 // Please have in mind to set block width as number divisible by number of frames per move, so you will avoid graphics issues
#define BLOCKHEIGHT 30
#define FRAMESPERMOVE 5
#define MOVELEFTACTION 2
#define SCREEN_WIDTH 1280
#define MOVERIGHTACTION 3
#define SCREEN_HEIGHT 800
#define LEADERBOARDSIZE 10
#define BASEMOVELATENCY 1000
#define SAVEFILE "zapis.txt"
#define LEADERBOARD "leaderboard.txt"

// Honestly, I have no idea why I put Point structure right here. Only because every single class which needs Point uses macros from list above,
// I decided not to removing it.
// Congrats to my stupidity while designing this part.
struct Point {
  int x;
  int y;
};

#endif // CONSTS_H
