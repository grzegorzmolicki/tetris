#ifndef AI_H
#define AI_H
#include "consts.h"
#include "currentblock.h"

int calculateAggregateHeight(int grid[ROWS][COLS]);
int checkLine(int grid[ROWS][COLS]);
int countHoles(int grid[ROWS][COLS]);
int countBumpiness(int grid[ROWS][COLS]);
long double calculateResult(int grid[ROWS][COLS]);
void makeAIMove(int grid[ROWS][COLS], CurrentBlock *block, SDL_Surface *screen, Player *player);

#endif // AI_H
