#ifndef CURRENTBLOCK_H
#define CURRENTBLOCK_H
#include "consts.h"
#include "player.h"

class CurrentBlock {
private:
  Point position;
  int brick[4][4];
  unsigned type;
  int colors[9];
  void generateBlock(int inputBlockCode);
  unsigned t0;
  unsigned actionToMake;
  unsigned rotations;
public:
  CurrentBlock(SDL_Surface *screen, int type);
  int getType();
  int getMaxRightIndex(int brick[4][4]);
  void lowerBlock(int grid[ROWS+1][COLS], Player *player, bool instantMove);
  void moveDown(int grid[ROWS+1][COLS], Player *player);
  void move(int grid[ROWS+1][COLS], int dir);
  void aiMove(int grid[ROWS+1][COLS], int dir);
  void drawBlock(SDL_Surface *screen);
  void chooseRandomBlock();
  void rotate(int grid[ROWS+1][COLS]);
  void addActionToMake(int action);
  void makeAction(int grid[ROWS+1][COLS], Player *player);
  bool hasActionToMake();
  void loadGame(Point position, unsigned stats[2]);
  void reset();
  unsigned getRotation();
  Point getPosition();
};

#endif // CURRENTBLOCK_H
