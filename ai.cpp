#include "ai.h"

int calculateAggregateHeight(int grid[ROWS][COLS]) {
  int aggregateHeight = 0;
  for(int i = 0; i < COLS; i++)
    for(int j = 0; j < ROWS; j++) {
        if(grid[j][i] != 0) {
            aggregateHeight += ROWS-j;
            break;
          }
      }
  return aggregateHeight;
}

int checkLine(int grid[ROWS][COLS]) {
  int lines = 0;
  for(int i = ROWS-1; i >= 0; i--) {
      bool wholeLine = true;
      for(int j = 0; j < COLS; j++) {
          if(grid[i][j] == 0) wholeLine = false;
        }
      if(wholeLine) {
          lines++;
          for(int j = 0; j < COLS; j++)
            grid[i][j] = 0;
          for(int k = i; k > 0; k--)
            for(int j = 0; j < COLS; j++)
              grid[k][j] = grid[k-1][j];
          i++;
        }
    }
  return lines;
}

int countHoles(int grid[ROWS][COLS]) {
  int holesCounter = 0;
  for(int i = 0; i < COLS; i++)
    for(int j = 0; j < ROWS-1; j++) {
        if(grid[j][i] != 0 && grid[j+1][i] == 0) {
            while(grid[j+1][i] == 0) {
                holesCounter++;
                j++;
              }
          }
      }
  return holesCounter;
}

int countBumpiness(int grid[ROWS][COLS]) {
  int heights[COLS];
  for(int i = 0; i < COLS; i++)
    heights[i] = 0;

  for(int i = 0; i < COLS; i++)
    for(int j = 0; j < ROWS; j++) {
        if(grid[j][i] != 0) {
            heights[i] = ROWS-j;
            break;
          }
      }
  int bumpiness = 0;
  for(int i = 0; i < COLS-1; i++) {
      bumpiness += abs(heights[i]-heights[i+1]);
    }
  return bumpiness;
}

long double calculateResult(int grid[ROWS][COLS]) {
  // The formula for result is as shown: result = a*aggregateHeight+b*numberofFullLines+c*holes+d*bumpiness;
//  const double aggregateHeightWage = -123;
//  const double fullLinesWage = 44000;
//  const double holesWage = -19000;
//  const double bumpinessWage = -2850;

  const long double aggregateHeightWage = -1.0202100834798195E9;
  const long double fullLinesWage = 4403977608022913E-4;
  const long double holesWage = -6.3668664386586E8;
  const long double bumpinessWage = -6.3668664386586E8;

  return (aggregateHeightWage*calculateAggregateHeight(grid))+(fullLinesWage*checkLine(grid))+(holesWage*countHoles(grid))+(bumpinessWage*countBumpiness(grid));
}

void makeAIMove(int grid[ROWS][COLS], CurrentBlock *block, SDL_Surface *screen, Player *player) {
  int bestRotations = 0;
  int bestRightMove = 0;
  long double bestResult;
  int tempGrid[ROWS+1][COLS];

  for(int rows = 0; rows < ROWS+1; rows++)
    for(int cols = 0; cols < COLS; cols++)
      tempGrid[rows][cols] = grid[rows][cols];
  CurrentBlock *tempBlock = new CurrentBlock(screen, block->getType());
  tempBlock->moveDown(tempGrid, player);
  bestResult = calculateResult(tempGrid);


  for(int rotations = 0; rotations < 4; rotations++) {
      for(int column = 0; column < COLS+2; column++) { // od lewa do prawa
          for(int rows = 0; rows < ROWS+1; rows++)
            for(int cols = 0; cols < COLS; cols++)
              tempGrid[rows][cols] = grid[rows][cols]; // kopiowanie tablicy rozgrywki do tymczasowej

          CurrentBlock *tempBlock = new CurrentBlock(screen, block->getType()); // obsługa obiektu klocka i przesunięcie go w lewo.
          for(int left = 0; left < COLS/2+2; left++)
            tempBlock->aiMove(tempGrid, DIRLEFT);

          for(int rot = 0; rot < rotations; rot++)
            tempBlock->rotate(tempGrid);

          for(int moves = 0; moves < column; moves++)
            tempBlock->aiMove(tempGrid, DIRRIGHT);
          tempBlock->moveDown(tempGrid, player);

          long double result = calculateResult(tempGrid);
          if(bestResult < result) {
              bestResult = result;
              bestRightMove = column;
              bestRotations = rotations;
            }
        }
    }

  for(int leftMoves = 0; leftMoves < COLS/2+2; leftMoves++)
    block->aiMove(grid, DIRLEFT);

  for(int rotations = 0; rotations < bestRotations; rotations++)
    block->rotate(grid);

  for(int rightMoves = 0; rightMoves < bestRightMove; rightMoves++)
    block->aiMove(grid, DIRRIGHT);
  block->addActionToMake(LOWERACTION);
}
