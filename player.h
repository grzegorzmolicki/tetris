#include <SDL2/SDL.h>

#ifndef PLAYER_H
#define PLAYER_H

class Player {
private:
  unsigned lastLines;
  unsigned linesCompleted;
  unsigned level;
  unsigned points;
  unsigned levelTimeCounter;
  unsigned timesSaved;

public:
  Player();
  void addPoints(int lines);
  void printStats(SDL_Surface *screen, SDL_Surface *charset);
  void nextLevel();
  void checkLevel();
  void nextSave();
  unsigned getLevel();
  unsigned getTimesSaved();
  unsigned getLastLines();
  unsigned getLinesCompleted();
  unsigned getPoints();
  unsigned getLevelTimeCounter();
  void reset();
  void loadGameStats(unsigned stats[6]);
};
#endif // PLAYER_H
